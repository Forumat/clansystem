package me.forumat.clansystem;

import com.mongodb.client.MongoCollection;
import lombok.Getter;
import me.forumat.clansystem.handler.message.MessageHandler;
import me.forumat.clansystem.handler.mongo.MongoHandler;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public final class ClanSystem extends JavaPlugin {

    @Getter private static ClanSystem instance;
    @Getter private MessageHandler messageHandler;
    @Getter private MongoHandler mongoHandler;
    @Getter private MongoCollection clanCollection;

    @Override
    public void onEnable() {
        instance = this;

        saveResource("message.yml", true);
        saveResource("config.yml", false);

        this.messageHandler = new MessageHandler(new File(getDataFolder(), "message.yml"), "prefix");

        String hostName = getConfig().getString("Mongo.Host");
        String userName = getConfig().getString("Mongo.User");
        String password = getConfig().getString("Mongo.Password");

        String mongoConnectionURI =
                "mongodb://" +
                        userName +
                        ":" +
                        password +
                        "@" +
                        hostName +
                        "/admin?retryWrites=true&w=majority";

        mongoHandler = new MongoHandler(mongoConnectionURI);
        mongoHandler.selectDatabase("clans");

        clanCollection = mongoHandler.getCollection("clanObjects");

    }

}
