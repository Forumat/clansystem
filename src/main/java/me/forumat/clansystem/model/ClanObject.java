package me.forumat.clansystem.model;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import lombok.AllArgsConstructor;
import lombok.Getter;
import me.forumat.clansystem.ClanSystem;
import me.forumat.clansystem.handler.mongo.MongoHandler;
import org.bson.Document;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@AllArgsConstructor
@Getter
public class ClanObject {

    private String name, tag;
    private ChatColor tagColor;
    private int maxMembers;
    private List<UUID> members;
    private JoinState joinState;

    private static List<ClanObject> clans = new ArrayList<>();
    private static MongoCollection mongoCollection = ClanSystem.getInstance().getClanCollection();
    private static MongoHandler mongoHandler = ClanSystem.getInstance().getMongoHandler();

    public static ClanObject getClan(String name){
        if(clans.stream().noneMatch(clan -> clan.getName().equalsIgnoreCase(name))){

            Document document = mongoHandler.getValue(mongoCollection, "clan_name", name.toUpperCase());
            String tag = document.getString("clan_tag");
            ChatColor tagColor = ChatColor.getByChar(document.getString("clan_color"));
            int maxMembers = document.getInteger("clan_maxmembers");
            List<UUID> members = document.getList("clan_members", UUID.class);
            JoinState joinState = JoinState.valueOf(document.getString("clan_joinstate"));

            return new ClanObject(name, tag, tagColor, maxMembers, members, joinState);

        }
        return clans.stream().filter(clan -> clan.getName().equalsIgnoreCase(name)).findFirst().get();
    }

    public void create(){
        Document document = new Document("clan_name", getName())
                .append("clan_tag", getTag())
                .append("clan_color", getTagColor())
                .append("clan_maxmembers", getMaxMembers())
                .append("clan_members", getMembers())
                .append("clan_joinstate", getJoinState().name());
        mongoCollection.insertOne(document);
    }

    public static boolean existsClan(String clanName){
        return mongoHandler.existsKey(mongoCollection, "clan_name", clanName.toUpperCase());
    }

    public static boolean isMemberOfAnyClan(UUID uuid){
        return false;
        // TODO: 18.03.2021  
    }

    public void setName(String newName){
        this.name = newName;
        mongoHandler.setValue(mongoCollection, "clan_name", getName(), "clan_name", newName);
    }

    public void join(UUID uuid){
        List<UUID> members = getMembers();
        members.add(uuid);

        this.members = members;
        mongoHandler.setValue(mongoCollection, "clan_name", getName(), "clan_members", members);
    }

    public void leave(UUID uuid){
        List<UUID> members = getMembers();
        members.remove(uuid);

        this.members = members;
        mongoHandler.setValue(mongoCollection, "clan_name", getName(), "clan_members", members);
    }

    public boolean isPlayerInClan(UUID uuid){
        List<UUID> members = getMembers();
        return members.contains(uuid);
    }

    public void setMaxMembers(int newMaxMembers){
        this.maxMembers = newMaxMembers;
        mongoHandler.setValue(mongoCollection, "clan_name", getName(), "clan_maxmembers", newMaxMembers);
    }

    public void setJoinState(String joinState){
        this.joinState = JoinState.valueOf(joinState.toUpperCase());
        mongoHandler.setValue(mongoCollection, "clan_name", getName(), "clan_joinstate", joinState);
    }

    public void setTag(String newTag){
        this.tag = newTag;
        mongoHandler.setValue(mongoCollection, "clan_name", getName(), "clan_tag", newTag);
    }

    public void setColor(String newColor){
        this.tagColor = ChatColor.getByChar(newColor);
        mongoHandler.setValue(mongoCollection, "clan_name", getName(), "clan_color", newColor);
    }

    public enum JoinState{
        OPEN,REQUEST,CLOSED;
    }


}
