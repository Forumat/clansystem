package me.forumat.clansystem.handler.message;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.regex.Matcher;

public class MessageHandler {

    private File file;
    private YamlConfiguration yamlConfiguration;
    private String prefixKey;


    public MessageHandler(File file, String prefixKey) {
        this.file = file;
        yamlConfiguration = YamlConfiguration.loadConfiguration(file);
        this.prefixKey = ChatColor.translateAlternateColorCodes('&', yamlConfiguration.getString(prefixKey));
    }

    public String getMessage(String messageKey, Object... args) {
        String message = yamlConfiguration.getString(messageKey);
        if (message == null) {
            message = "§cNachricht " + messageKey + " wurde nicht gefunden!";
        }
        message = message.replace("%P%", prefixKey);
        if (args.length != 0) {
            int i = 0;
            for (Object arg : args) {
                if (arg != null) {
                    message = message.replaceAll("\\{" + i + "}", Matcher.quoteReplacement(arg.toString()));
                    i++;
                }
            }
        }
        return ChatColor.translateAlternateColorCodes('&', message);
    }


    public void sendMessage(CommandSender commandSender, String key, Object... replacements){
        commandSender.sendMessage(getMessage(key, replacements));
    }

}
